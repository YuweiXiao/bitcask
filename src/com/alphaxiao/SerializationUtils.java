package com.alphaxiao;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class SerializationUtils {
    
    public static long CRC32(byte[] data) {
        Checksum checksum = new CRC32();
        checksum.update(data, 0, data.length);
        return checksum.getValue();
    }

    public static boolean validateCRC32(long crc, byte[] data) {
        return CRC32(data) == crc;
    }

    // TODO use little endian or native order?
    public static byte[] toBytes(Integer value) {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array();
    }

    public static Integer toInteger(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public static byte[] toBytes(Record record) {
        ByteBuffer buffer = ByteBuffer.allocate(24).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putLong(0, record.crc);
        buffer.putInt(8, record.tstamp);
        buffer.putInt(12, record.keySize);
        buffer.putInt(16, record.valueSize);
        buffer.putInt(20, record.key);
        return buffer.array();
    }

    public static Record toRecord(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return new Record(buffer.getLong(0), buffer.getInt(8), buffer.getInt(12), buffer.getInt(16), buffer.getInt(20));
    }

    // size of hint file record in bytes
    public static final int HINT_FILE_RECORD_SIZE = 16;

    public static byte[] toHintBytes(Record record, int offset) {
        ByteBuffer buffer = ByteBuffer.allocate(HINT_FILE_RECORD_SIZE).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(0, record.tstamp);
        buffer.putInt(4, record.valueSize);
        buffer.putInt(8, offset);
        buffer.putInt(12, record.key);
        return buffer.array();
    }

    // Read hint file bytes, and fill in dirRecord
    // Return key
    public static int toDirRecord(DirRecord dirRecord, byte[] bytes, int offset) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes, offset, HINT_FILE_RECORD_SIZE).order(ByteOrder.LITTLE_ENDIAN);
        dirRecord.tstamp = buffer.getInt(0);
        dirRecord.valueSize = buffer.getInt(4);
        dirRecord.offset = buffer.getInt(8);
        return buffer.getInt(12);
    }

    public static int toDirRecord(DirRecord dirRecord, byte[] bytes) {
        return toDirRecord(dirRecord, bytes, 0);
    }

    public static byte[] toBytes(Object object) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(object);
            return bos.toByteArray();
        } catch(IOException e) {
            System.out.println("Fail to convert obj into bytes array: " + e.getMessage());
        }
        return new byte[0];
    }

    public static <T> T toObjDefault(byte[] bytes) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInput in = new ObjectInputStream(bis);

            return (T) in.readObject();
        } catch (Exception e) {
            System.out.format("Fail to convert bytes to obj, %s", e.getMessage());
        }
        return null;
    }
}
