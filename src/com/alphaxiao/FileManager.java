package com.alphaxiao;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

class Record {

    public static final int RECORD_OBJ_SIZE = SerializationUtils.toBytes(new Record(0, 0, 0, 0, 0)).length;

    public Record(long crc, int tstamp, int keySize, int valueSize, int key) {
        this.crc = crc;
        this.tstamp = tstamp;
        this.keySize = keySize;
        this.valueSize = valueSize;
        this.key = key;
    }

    public long crc;
    public int tstamp;
    public int keySize;
    public int valueSize;
    public int key;
}

class DirRecord {

    public DirRecord(int fileID) {
        this.fileID = fileID;
    }

    public DirRecord(int fileID, int offset) {
        this.fileID = fileID;
        this.offset = offset;
    }

    public DirRecord(int fileID, int offset, int valueSize, int tstamp) {
        this.fileID = fileID;
        this.offset = offset;
        this.valueSize = valueSize;
        this.tstamp = tstamp;
    }

    public int fileID;
    public int offset;
    public int valueSize;
    public int tstamp;
}

class FileBlock {

    public static final String TMP_DATA_FILE_PREFIX = "TMP_";
    public static final String HINT_FILE_POSTFIX = ".hint";
    public static final String FILE_POSTFIX = ".bitcask";
    public static int META_DATA_SIZE = 32;
    private static int BLOCK_ID_INDEX = 0;
    private static int BLOCK_OFFSET_INDEX = 4;

    // Merge file blocks. This method is used in restart process
    // 由于重启时还没有 keydir，无法知道 record 是否过期，因此默认所有 record 都是 valid。
    public static List<FileBlock> mergeFileBlocks(List<FileBlock> blocks, Callable<FileBlock> allocateFileBlock) {
        if(blocks.size() <= 1) {
            return blocks;
        }
        List<FileBlock> lBlocks = mergeFileBlocks(blocks.subList(0, blocks.size()/2), allocateFileBlock);
        List<FileBlock> rBlocks = mergeFileBlocks(blocks.subList(blocks.size()/2, blocks.size()), allocateFileBlock);
        List<FileBlock> ret = new ArrayList<FileBlock>();

        // TODO sort blocks based on blockOffset

        int l = 0, r = 0;
        while(l < lBlocks.size() && r < rBlocks.size()) {
            FileBlock lBlock = lBlocks.get(l);
            FileBlock rBlock = rBlocks.get(r);
            if(lBlocks.get(l).remaining() >= rBlocks.get(r).getBlockOffset() - META_DATA_SIZE) {
                try {
                    FileBlock block = allocateFileBlock.call();
                    block.write(lBlock.read(META_DATA_SIZE, lBlock.getBlockOffset() - META_DATA_SIZE), META_DATA_SIZE);
                    block.write(rBlock.read(META_DATA_SIZE, rBlock.getBlockOffset() - META_DATA_SIZE), lBlock.getBlockOffset());
                    block.setBlockOffset(lBlock.getBlockOffset() + rBlock.getBlockOffset() - META_DATA_SIZE);
                    ret.add(block);
                } catch (Exception e) {
                    // TODO exception handling
                }
            } else {
                ret.add(lBlock);
                ret.add(rBlock);
            }
            l += 1;
            r += 1;
        }
        for (; l < lBlocks.size(); ++l)
            ret.add(lBlocks.get(l));
        for (; r < rBlocks.size(); ++r)
            ret.add(rBlocks.get(r));
        return ret;
    }

    // TODO adaptive compaction
    public static FileBlock compactBlock(FileBlock block, Callable<FileBlock> allocateFileBlock) {
        try {
            var newBlock = allocateFileBlock.call();
            return newBlock;
        } catch (Exception e) {

        }
        return block;
    }

    public FileBlock(String filename, int id, int blockSize) {
        this.filename = filename;
        this.id = id;

        try {
            RandomAccessFile rf = new RandomAccessFile(this.filename, "rw");
            buffer = rf.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, blockSize);

            blockOffset = (int) buffer.getInt(BLOCK_OFFSET_INDEX);

            if (blockOffset == 0) { // equals zero means there is no existing data file (mmap create zero-filled buffer)
                blockOffset = META_DATA_SIZE;
                buffer.putInt(BLOCK_ID_INDEX, this.id);
                buffer.putInt(BLOCK_OFFSET_INDEX, blockOffset);
            } else {
                this.id = buffer.getInt(BLOCK_ID_INDEX);
            }
        } catch (Exception e) {
            System.out.format("Fail to read data file: %s, error: %s \n", this.filename, e.getMessage());
        }

        // System.out.println("block offset: " + blockOffset);
        dirty = false;
    }

    // Not thread-safe, the caller should manage concurrency calling to this method.
    public int write(byte[] data) {
        if(data == null || data.length == 0) 
            return blockOffset;
        if (data.length + blockOffset > buffer.limit()) {
            System.out.format("Overflow of file block. block id: %d, data length: %d, remaining slot: %d \n", this.id,
                    data.length, remaining());
            return -1;
        }
        int retOffset = blockOffset;
        blockOffset += data.length;
        this.write(data, retOffset);
        buffer.putInt(BLOCK_OFFSET_INDEX, blockOffset);
        dirty = true;

        // System.out.println("block offset: " + blockOffset);
        return retOffset;
    }

    private int write(byte[] data, int writeOffset) {
        for (int i = 0; i < data.length; ++i)
            buffer.put(writeOffset + i, data[i]);
        dirty = true;
        return writeOffset;
    }

    public byte[] read(int offset, int size) {
        if (size <= 0)
            return new byte[0];
        if (offset + size > blockOffset) {
            System.out.format("Reading goes out side of block. block id: %d offset: %d, size: %d, blockOffset: %d \n", getID(), offset, size, blockOffset);
            return null;
        }
        byte[] ret = new byte[size];
        for (int i = 0; i < size; ++i)
            ret[i] = buffer.get(offset + i);
        return ret;
    }

    public void sync() {
        if (!dirty)
            return;
        try {
            buffer.putInt(BLOCK_OFFSET_INDEX, blockOffset);
            buffer.force();
            dirty = false;
        } catch (Exception e) {
            System.out.println("Fail to operate file: " + e.getMessage());
        }
    }

    // Load hint file and update in-memory index: keydir
    // Return the maximum time stamp 
    public int loadHintAndUpdateIndex(Map<Integer, DirRecord> keydir) {
        String hintFilename = getHintFilename();
        int maxTStamp = 0;
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(hintFilename));
            byte[] data = new byte[SerializationUtils.HINT_FILE_RECORD_SIZE];
            while (in.available() > 0) {
                int count = in.read(data);
                if (count == 0)
                    break;
                DirRecord dirRecord = new DirRecord(getID());
                int key = SerializationUtils.toDirRecord(dirRecord, data);
                System.out.format("Recover key from hint, key: %d, tstamp: %d, offset: %d%n", key, dirRecord.tstamp, dirRecord.offset);
                if(keydir.containsKey(key)) {
                    if(Math.abs(dirRecord.tstamp) > keydir.get(key).tstamp) {
                        keydir.put(key, dirRecord);
                    }
                } else {
                    keydir.put(key, dirRecord);
                }
                maxTStamp = Math.max(maxTStamp, Math.abs(dirRecord.tstamp));
            }
        } catch (Exception e) {
            System.out.format("Fail to initialize from hint file %s. Error: %s, %n", hintFilename, e.getMessage());
            return -1;
        }
        return maxTStamp;
    }

    // Read the whole block and generate hint file, and update in-memory index: keydir
    public int generateHintAndUpdateIndex(Map<Integer, DirRecord> keydir) {
        int maxTStamp = 0;
        int offset = META_DATA_SIZE;
        String hintFilename = getHintFilename();
        try {
            OutputStream out = new BufferedOutputStream(new FileOutputStream(hintFilename, false));
            
            while (offset < getBlockOffset()) {
                Record record = SerializationUtils.toRecord(read(offset, Record.RECORD_OBJ_SIZE));
                
                if (record == null)
                    break;
                
                boolean isMoreRecent = !keydir.containsKey(record.key) ? true : Math.abs(record.tstamp) > keydir.get(record.key).tstamp;
                
                System.out.format("Recover key: %d, tstamp: %d, isRecent: %d%n", record.key, record.tstamp, isMoreRecent ? 1: 0);
                if(isMoreRecent) {
                    keydir.put(record.key, new DirRecord(getID(), offset, record.valueSize, record.tstamp));
                    out.write(SerializationUtils.toHintBytes(record, offset));
                }
                
                maxTStamp = Math.max(maxTStamp, record.tstamp);
                offset += Record.RECORD_OBJ_SIZE + record.valueSize;
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxTStamp;
    }

    public void deleteDatafile() {
        FileUtils.deleteFile(getFilename());
        FileUtils.deleteFile(getHintFilename());
    }

    public String getHintFilename() {
        return getFilename().replace(FILE_POSTFIX, HINT_FILE_POSTFIX);
    }

    public boolean hasHintFile() {
        return new File(getHintFilename()).exists();
    }

    public int remaining() {
        return buffer.capacity() - blockOffset;
    }

    public int getID() {
        return this.id;
    }

    public String getFilename() {
        return filename;
    }

    public int getBlockOffset() {
        return blockOffset;
    }

    public void setBlockOffset(int bo) {
        blockOffset = bo;
        buffer.putInt(BLOCK_OFFSET_INDEX, blockOffset);
    }

    private boolean dirty = false;
    private String filename;
    private int id;
    private int blockOffset;
    private MappedByteBuffer buffer;
}

public class FileManager {

    public static final int MAX_BLOCK_SIZE = 16 * 1024 * 1024; // 4M

    public FileManager(String path) {
        this.path = path;
        blockIDcounter = 0;
        tstampCounter = new AtomicInteger(0);
        blocks = new HashMap<Integer, FileBlock>();
        oldBlocks = new ArrayList<FileBlock>();
        activeBlocks = new ConcurrentLinkedQueue<FileBlock>();
    }

    // Initialize by reading data from disk, and update in-memory index: keydir
    public void initialize(Map<Integer, DirRecord> keydir) {
        File fp = new File(path);
        fp.mkdirs(); // incase the path doesn't exist

        // get existing data files
        List<String> dataFiles = new ArrayList<String>();
        for (String pathname : fp.list()) {
            if (pathname.endsWith(FileBlock.FILE_POSTFIX)) {
                dataFiles.add(pathname);
            }
        }

        List<FileBlock> mergeCandidates = new ArrayList<FileBlock>();
        for (String dataFile : dataFiles) {
            FileBlock block = new FileBlock(Paths.get(path, dataFile).toString(), -1, MAX_BLOCK_SIZE); // The file id should be initialized by block itself from disk
            if(block.hasHintFile()) {
                int mTimestamp = block.loadHintAndUpdateIndex(keydir);
                tstampCounter.set(Math.max(tstampCounter.get(), mTimestamp));
                blocks.put(block.getID(), block);
                oldBlocks.add(block);
            } else {
                mergeCandidates.add(block);
            }
            blockIDcounter = Math.max(blockIDcounter, block.getID() + 1);
        }

        var mergedBlockes = FileBlock.mergeFileBlocks(mergeCandidates, new Callable<FileBlock>() {
            @Override
            public FileBlock call() {
                int nextCounter = getNextCounter();
                return new FileBlock(Paths.get(path, FileBlock.TMP_DATA_FILE_PREFIX + nextCounter + FileBlock.FILE_POSTFIX).toString(), nextCounter, MAX_BLOCK_SIZE);
            }
        });

        // read merged block, generate their hint files and update keydir index
        for(FileBlock b: mergedBlockes) {
            String filename = b.getFilename().replace(FileBlock.TMP_DATA_FILE_PREFIX, "");
            new File(b.getFilename()).renameTo(new File(filename));
            FileBlock block = new FileBlock(filename, b.getID(), MAX_BLOCK_SIZE);
            int mTimestamp = block.generateHintAndUpdateIndex(keydir);
            tstampCounter.set(Math.max(tstampCounter.get(), mTimestamp));
            blocks.put(block.getID(), block);
            oldBlocks.add(block);
        }

        // clean up merge candidates & tmp data files
        for(FileBlock b: mergeCandidates) {
            if(blocks.containsKey(b.getID())) // the block is reused
                continue;
            b.deleteDatafile();
        }
        for (String pathname : fp.list()) {
            if (pathname.startsWith(FileBlock.TMP_DATA_FILE_PREFIX)) {
                FileUtils.deleteFile(Paths.get(path, pathname).toString());
            }
        }

        while(activeBlocks.size() < 4) {    // TODO make the number of active files configurable
            var block = newActiveFileBlock();
            activeBlocks.offer(block);
        }
    }

    public void close() {
        flush();
    }

    public void flush() {
        for (FileBlock fileBlock : activeBlocks) {
            fileBlock.sync();
        }
    }

    // Append new record (and data) into file block
    // Return the memory index record
    public DirRecord append(int key, byte[] data) {
        Record record = new Record(SerializationUtils.CRC32(data), tstampCounter.getAndIncrement(), 4, data.length, key);
        return append(record, data);
    }

    public byte[] get(int fileID, int offset, int valueSize) {
        if (!blocks.containsKey(fileID)) {
            System.out.println("Cannot find block with id: " + fileID);
            return null;
        }

        return blocks.get(fileID).read(offset + Record.RECORD_OBJ_SIZE, valueSize);
    }

    // TODO update statistics in old file block, to assist compaction
    public void delete(int key) {
        Record record = new Record(0, -tstampCounter.getAndIncrement(), 4, 0, key);
        append(record, null);
    }

    /** 
    // Compact blocks[i] by removing deleted and old records
    // i: block index
    public void compactBlock(int i, Map<Integer, DirRecord> keydir, int offset, int availableSlot) {
        FileBlock block = blocks.get(i);
        int fileID = block.getID();
        if(offset < 0) {
            offset = FileBlock.META_DATA_SIZE;
            availableSlot = offset;
        }
        while (offset < block.getBlockOffset()) {
            Record r = getRecord(fileID, offset);
            if(keydir.containsKey(r.key) && keydir.get(r.key).tstamp >= 0) {
                if (availableSlot != offset) {
                    block.write(block.read(offset, RECORD_OBJ_SIZE + r.valueSize), availableSlot);
                    keydir.get(r.key).offset = availableSlot;
                }
                availableSlot += RECORD_OBJ_SIZE + r.valueSize;
            }
            offset += RECORD_OBJ_SIZE + r.valueSize;
        }
        block.setBlockOffset(availableSlot);
    }

    // Merge old file blocks
    // 1. 同时做 compaction 和 merge，且只针对 old files 做，不干扰正在对 active file 进行的 read/write/delete
    // 2. 根据 keydir 的信息直接判定 record 是否保留，且在移动 record 后，同步更新 keydir 中的 fileID 与 offset。
    // 3. merge 的同时，直接利用 keydir 生成 hint file
    // 4. 删除空的 old files
    // Assume all records in blocks are valid
    public void mergeBlocks(Map<Integer, DirRecord> keydir) {
        if(blocks.size() <= 1) return;

        int l = 0;
        int r = blocks.size() - 2;
        int rBlockOffset = FileBlock.META_DATA_SIZE;
        compactBlock(l, keydir, FileBlock.META_DATA_SIZE, FileBlock.META_DATA_SIZE);
        while (l < r) {
            if (rBlockOffset >= blocks.get(r).getBlockOffset()) {
                r -= 1;
                rBlockOffset = FileBlock.META_DATA_SIZE;
                continue;
            }

            Record record = getRecord(blocks.get(r).getID(), rBlockOffset);
            // skip outdated record
            if(!keydir.containsKey(record.key) || record.tstamp != keydir.get(record.key).tstamp) {
                rBlockOffset += RECORD_OBJ_SIZE + record.valueSize;
                continue;
            }
            if (blocks.get(l).remaining() >= RECORD_OBJ_SIZE + record.valueSize) {
                int offset = blocks.get(l).write(blocks.get(r).read(rBlockOffset, RECORD_OBJ_SIZE + record.valueSize));
                keydir.get(record.key).fileID = blocks.get(l).getID();
                keydir.get(record.key).offset = offset;
                rBlockOffset += RECORD_OBJ_SIZE + record.valueSize;
            } else {
                l += 1;
                if(l < r) compactBlock(l, keydir, FileBlock.META_DATA_SIZE, FileBlock.META_DATA_SIZE);
            }
            // System.out.format("l: %d, r: %d, offset: %d %n", l, r, rBlockOffset);
        }

        // handle the last block, do compaction if there is still records
        boolean keepLast = false;
        if (rBlockOffset < blocks.get(r).getBlockOffset()) {
            keepLast = true;
            compactBlock(r, keydir, rBlockOffset, FileBlock.META_DATA_SIZE);
        }
        if(blocks.get(r).getBlockOffset() == FileBlock.META_DATA_SIZE) 
            keepLast = false;

        // delete files that have been merged and remove them from the block list
        for (int i = r + (keepLast ? 1 : 0); i < blocks.size() - 1; ++i) {
            FileUtils.deleteFile(blocks.get(i).getFilename());
        }
        blocks.subList(r + (keepLast ? 1 : 0), blocks.size() - 1).clear();

        // System.out.format("l: %d, r: %d, keep last: %d %n", l, r, keepLast?1:0);

        constructBlockIDToIndex();
    }

    // Generate hint file for old data blocks, based on given keydir
    public void generateHintFiles(Map<Integer, DirRecord> keydir) {
        for (int i = 0; i < blocks.size() - 1; ++i) {
            FileBlock block = blocks.get(i);
            int fileID = block.getID();
            int offset = FileBlock.META_DATA_SIZE;
            String hintFilename = getHintFilenameByIndex(i);
            try {
                // overwrite existing hint file
                OutputStream out = new BufferedOutputStream(new FileOutputStream(hintFilename, false));
                while (offset < block.getBlockOffset()) {
                    Record r = getRecord(fileID, offset);
                    if(keydir.containsKey(r.key) && keydir.get(r.key).tstamp == r.tstamp) {
                        out.write(SerializationUtils.toHintBytes(r, offset));
                    }
                    offset += RECORD_OBJ_SIZE + r.valueSize;
                }
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
*/

    public int numOfBlock() {
        return blocks.size();
    }

    public int getTStampCounter() {
        return tstampCounter.get();
    }

    public void setTStampCounter(int v) {
        tstampCounter.set(v);
    }

    @Override
    public String toString() {
        String ret = "";
        for (FileBlock block : oldBlocks) {
            ret += String.format("(old file) id: %d, filename: %s, blockOffset: %d %n", block.getID(), block.getFilename(), block.getBlockOffset());
        }
        for (FileBlock block : activeBlocks) {
            ret += String.format("(active file) id: %d, filename: %s, blockOffset: %d %n", block.getID(), block.getFilename(), block.getBlockOffset());
        }
        return ret;
    }

    // Append metadata and data to active file block. If the current active file block is full, then 
    private DirRecord append(Record record, byte[] data) {
        byte[] metadata = SerializationUtils.toBytes(record);
        int dataLength = data == null ? 0 : data.length;
        FileBlock block = getActiveFileBlock(metadata.length + dataLength);
            
        int offset = block.write(metadata);
        block.write(data);
        var ret = new DirRecord(block.getID(), offset, dataLength, record.tstamp);
        activeBlocks.offer(block);
        return ret;
    }

    private FileBlock getActiveFileBlock(int sizeToWrite) {
        FileBlock block = activeBlocks.poll();
        while(block == null) { // TODO better wait mechanism if there is no active block usable
            block = activeBlocks.poll();
        }
        if(block.remaining() < sizeToWrite) {
            synchronized(oldBlocks) {
                oldBlocks.add(block);
            }
            block = newActiveFileBlock();
        }
        return block;
    }

    private int getNextCounter() {
        while (blocks.containsKey(blockIDcounter))
            blockIDcounter += 1;
        int ret = blockIDcounter;
        blockIDcounter += 1;
        return ret;
    }

    private synchronized FileBlock newActiveFileBlock() {
        int nextCounter = getNextCounter();
        FileBlock block = new FileBlock(Paths.get(this.path, nextCounter + FileBlock.FILE_POSTFIX).toString(),
                nextCounter, MAX_BLOCK_SIZE);
        assert nextCounter == block.getID();
        blocks.put(block.getID(), block);
        return block;
    }

    private Integer blockIDcounter;             // used to generate new fileID 
    private AtomicInteger tstampCounter;        // global increasing time stamp
    private String path;                        // data directory

    private Map<Integer, FileBlock> blocks;     // mapping between fildID (blockID) -> block object, contain all file blocks
    private List<FileBlock> oldBlocks;          // old file blocks
    private ConcurrentLinkedQueue<FileBlock> activeBlocks;  // active file blocks
};
