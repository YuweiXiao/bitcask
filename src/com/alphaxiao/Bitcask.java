package com.alphaxiao;

import java.io.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Bitcask {

    public Bitcask() {
    }

    public Bitcask(String path) {
        open(path);
    }

    public void open(String path) {
        keydir = new ConcurrentHashMap<Integer, DirRecord>();
        fileManager = new FileManager(path);
        fileManager.initialize(keydir);

        // constructKeydirFromFileBlocks();
        System.out.format("time stamp counter: %d%n", fileManager.getTStampCounter());
    }

    public void close() {
        fileManager.close();
        keydir = null;
    }

    public void put(int key, byte[] data) {
        DirRecord dirRecord = fileManager.append(key, data);
        keydir.put(key, dirRecord);
    }

    public byte[] get(int key) {
        if (!keydir.containsKey(key)) {
            return null;
        }
        DirRecord dirRecord = keydir.get(key);
        if(dirRecord.tstamp < 0) 
            return null;
        return fileManager.get(dirRecord.fileID, dirRecord.offset, dirRecord.valueSize);
    }

    public void delete(int key) {
        if (!keydir.containsKey(key)) {
            return;
        }
        keydir.remove(key);
        fileManager.delete(key);
    }

    public void sync() {
        fileManager.flush();
    }

    // public void merge() {
    //     fileManager.mergeBlocks(keydir);
    //     fileManager.generateHintFiles(keydir);
    // }

    public void fold() {
        // TODO
    }

/** 
    // Read all records in fileManager to reconstruct the keydir in the memory
    private void constructKeydirFromFileBlocks() {
        // keydir = new HashMap<Integer, DirRecord>();
        keydir = new ConcurrentHashMap<Integer, DirRecord>();
        int maxTStamp = 0;

        for (int i = 0; i < fileManager.numOfBlock(); ++i) {
            int offset = FileBlock.META_DATA_SIZE;
            FileBlock block = fileManager.getFileBlockByIndex(i);
            int fileID = block.getID();
            if (fileManager.hasHintFile(fileID)) { // read keydir directly from hint file
                String hintFilename = fileManager.getHintFilenameByIndex(i);
                System.out.format("Initialize file block: %d from hint file: %s.%n", fileID, hintFilename);
                try {
                    InputStream in = new BufferedInputStream(new FileInputStream(hintFilename));
                    byte[] data = new byte[SerializationUtils.HINT_FILE_RECORD_SIZE];
                    while (in.available() > 0) {
                        int count = in.read(data);
                        if (count == 0)
                            break;
                        DirRecord dirRecord = new DirRecord(fileID);
                        int key = SerializationUtils.toDirRecord(dirRecord, data);
                        keydir.put(key, dirRecord);
                        maxTStamp = Math.max(maxTStamp, dirRecord.tstamp);
                        // System.out.format("%d, fileID: %d, offset:%d, tstamp: %d", key, dirRecord.fileID, dirRecord.offset, dirRecord.tstamp);
                    }
                } catch (Exception e) {
                    System.out.format("Fail to initialize from hint file %s. Error: %s, %n", hintFilename, e.getMessage());
                }
            } else {
                while (offset < block.getBlockOffset()) {
                    Record record = fileManager.getRecord(fileID, offset);
                    if (record == null)
                        break;
                    
                    boolean isMoreRecent = !keydir.containsKey(record.key) ? true : Math.abs(record.tstamp) > keydir.get(record.key).tstamp;
                    if(!isMoreRecent) continue;
                    
                    if (record.tstamp >= 0) {
                        keydir.put(record.key, new DirRecord(fileID, offset, record.valueSize, record.tstamp));
                    } else if (record.tstamp < 0) {    // negative time stamp means deleted
                        keydir.remove(record.key);
                    }
                    maxTStamp = Math.max(maxTStamp, record.tstamp);
                    offset += FileManager.RECORD_OBJ_SIZE + record.valueSize;
                }
            }
        }

        // recover time stamp, to ensure increasing order
        fileManager.setTStampCounter(maxTStamp);
    }
*/
    @Override
    public String toString() {
        return "Bitcask{" + "keydir count =" + keydir.size() + ", fileManager = " + fileManager + '}';
    }

    private Map<Integer, DirRecord> keydir;
    private FileManager fileManager;

}