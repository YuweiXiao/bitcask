package com.alphaxiao;

import java.security.SecureRandom;

public class RandomGenerator {

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    private String data;
    private int pos;

    public RandomGenerator() {
        this(1024 * 1024); // 1M
    }

    public RandomGenerator(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        data = sb.toString();
        pos = 0;
    }

    String generate(int len) {
        if (pos + len > data.length()) {
            pos = 0;
        }
        pos += len;
        return data.substring(pos - len, pos);
    }
}
