# Bitcask

Implementation of key-value storage engine of [bitcask](https://gitlab.com/YuweiXiao/bitcask/-/blob/master/doc/bitcask-intro.pdf) using Java.



## TODO

- [ ] data directory lock, allow only one process modifying the data directory
- [ ] 借助 BytesPool 降低 byte array allocation
- [ ] check lucene 中 IO API 用的是啥
- [ ] Java MappedByteBuffer 似乎不是一个好的 IO 选项（虽然读入是 laze 的，无法控制资源释放），尝试使用 BufferedOutputStream 和 BufferedInputStream
- [ ] support limited memory case, file blocks are read from disk rather than cached in memory
- [ ] read config from config file
- ~~[ ] metadata file, storing all meta information of bitcask~~
- [ ] (use integer for now) ~~support arbitrary key (use generic to serialize obj into byte array)~~
- [x] hint file 生成提供一个不依赖 keydir index 的版本，以支持重启时生成 hint file 并同时 fill keydir index。
- [x] crc check, to ensure validity of writing
- [x] add a marker to keydir, to indicate deleted record, or directly remove the key from keydir
- [x] check crash? may have data lose? (related to mmap io flush granularity)
- [x] check how useful dirty flag is in the mmap setup - has effect (1.3s -> 0.6s)
- [x] file block id and mapping between array index and block id
- [x] merge of files
- [x] restart
- [x] use mmap in FileBlock to manage memory data and disk data
- [x] design how to return result, status and query result
- [x] benchmark test code
- [x] support arbitrary value 
- [x] junit test framework
- [x] dirty flag for FileBlock (avoid extra write to disk when do syncing)
- [x] store files into disk

Parallel related:

- [x] Fix merge & hint file generation
- [ ] Read code to identify critical region
- [ ] parallel benchmark code
- [x] support parallel read & add test case
- [x] support parallel write & add test case

Benchmark related:

- [ ] data generator

## 欣扬改进意见

1. 内存索引DirRecord的结构设计需再精简，因为索引信息全内存，因此需要将fileId、offset等组装成byte数组进一步节省内存；
2. 考虑系统设计时支持的最大kv长度以及每个FileBlock的大小，从而精准设计fileID、offset、valueSize的精确占用位长度；(比如最大支持16MB长度的KV，同时单机引擎最大支持256GB大小)

    * 目前仅支持 key 为 integer，即 4 byte。所以当最大支持 16M KV 时，max(valueSize) = 16 * 1024 * 1024 - 4 = 2^24 - 4，需要 24 位来存储。
    * 假设每个 file 最大 256 MB，则当单机最大支持 256GB 时，max(fileID) = 1024 = 2^10, 需要至多 10 位来存储。
    * max(offset) = 256 * 1024 * 1024 = 2^28，需要至多28位来存储。
    * num_bit(fileID) + num_bit(valueSize) + num_bit(offset) = 62
    * 加上 integer time stamp，DirRecord 总共需要 62+32 = 94 bit。考虑最齐的问题，最终应该用 96 (32*3) bit。

3. 考虑更加优雅的删除实现方式，可采用顺序写，尽量避免随机写；(当前实现为写入timestamp为-1的记录)
4. compaction时结合删除实现方式，尽量使用索引实现，而不是重新读一次所有FileBlock再重新写一次(删除过多时存在IO浪费)；

    - [x] 顺序写 delete 操作 （写入一个 time stamp 为负的空数据）
    - [ ] compaction 操作的粒度为单个 old file block，如果控制好单个 file block 大小上限，且充分考虑隔离 old file 与 active file，应当能够做到不影响正常读写的情况下做 compaction。

5. compaction策略需要系统自适应，比如达到多少删除率时自动进行compaction，注意控制compaction速度，避免compaction造成系统的正常读写性能下降；

    - [ ] 记录每一个 file block 内 KV 个数，同时记录已删除的个数。当已删除个数较多时，后台自动进行 compaction。
    * compaction 过程中会修改 keydir 索引，需要仔细考虑 keydir 数据结构的 data race，最大限度降低对正常读写性能的影响。
    * compaction 仅对 old file block 进行，减少对正常读写的干预（正常读写只修改 active file）。

6. 读写需考虑并发，即多个读多个写线程同时存在时，如何高效进行用户的读写请求处理；
7. 内存索引可实现高效的分段式桶结构，可以参考ConcurrentHashMap实现(可直接使用该数据结构，但需深入理解实现机制)；

    - [x] 替换 ConcurrentHashMap，支持并行 hash table。
    - [x] 学习 ConcurrentHashMap

8. 考虑crash recover时，如何快速恢复出key内存索引，加速重启速度；(比如单机引擎达到100GB+时，重启速度会非常慢，考虑基于内存索引的checkpoint机制)

    - [x] generate hint files and use existing hint files during restart
    - [x] merge 与 generate hint files 两个操作，会修改 old files。若在修改过程中发生 crash，会导致旧数据损坏。因此需要额外生成一个拷贝，在操作结束后，原子 rename (swap) 保证数据完整性。
    * 分析
      * 假设一个 data file 256M 大小，一个 KV 大小 128 byte，则 KV 个数为 256 * 1024 * 1024 / 128 = 2M 个。重启单机至多需要读 (sizeof(DirRecord) + sizeof(key)) * 2M * 1024 个 data file = 16 (byte) * 2G = 32G。
      * 若使用 SSD 作为存储介质，完全重建内存索引需要的时间为 32G / 500M = 64s = 1 分钟。（假设 bottleneck 为 IO）
      * 进一步，如果优化 hint file 为 lazy 读取，即重启时，只读取 key，但暂不完全读取 DirRecord 信息，那么重启单机至多需要读 sizeof(key) * 2M * 1024 = 8G，重启满存储的单机则需要 15 s。
      * 只针对 old file 生成 hint file。只要 old file 不被修改（被merge），则对应的 hint file 就是有效的。但仍要考虑在 merge 过程中发生 crash，即 old file 与 hint file 之间的映射关系在 merge 过程中会过期，需要标记 hint file 或者其他 flag 以防止重启过程中过使用错误的 hint file。

## Crash Recovery

bitcask crash 之后，重启仅依靠存储在磁盘上的 data file 文件。因此在运行过程中，只要数据被持久化到了 data file，中断重启后都应该能够被恢复。

1. 如果系统可能在任意时候中断（写进行到一半），如何保证 data file integrity，即 data file 内的数据组织符合规范。

    * [ ] 加入 crc 检查，但仍需要保证 record metadata 写入是原子的。

2. bitcask 若返回 client ack，则代表此次操作完成，相关的数据应当能够在中断后恢复。那么是否意味着，bitcask 中每一次对 data file 的修改都要 flush IO 以保证持久化？

## Parallel

* read
* delete
* put

## Note

![note](./doc/note.jpeg)