package com.alphaxiao;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class BitcaskTest {
    static String path = "./data/";

    @Test
    void hello() {
        FileUtils.deleteFolder(path);
        Bitcask bitcask = new Bitcask(path);

        bitcask.put(1, SerializationUtils.toBytes(2));
        bitcask.put(2, SerializationUtils.toBytes(3));
        bitcask.put(3, SerializationUtils.toBytes(4));
        bitcask.put(128, SerializationUtils.toBytes(128));

        System.out.println(bitcask);

        assert SerializationUtils.toInteger(bitcask.get(1)) == 2 : "should equal 2";
        assert SerializationUtils.toInteger(bitcask.get(2)) == 3 : "should equal 3";
        assert SerializationUtils.toInteger(bitcask.get(3)) == 4 : "should equal 4";

        bitcask.delete(1);
        assert bitcask.get(1) == null : "should equal null";
        assert bitcask.get(4) == null : "key 4: should equal null";
        
        bitcask.put(3, SerializationUtils.toBytes(8));

        bitcask.sync();

        System.out.println(bitcask);

        // TODO add more data to have multiple file block

        helloRestart(0);
    }

    void helloRestart(int count) {
        System.out.format("Restart %d%n", count);
        Bitcask bitcask = new Bitcask(path);
        System.out.println(bitcask);

        assert bitcask.get(1) == null : "should equal null";
        assert bitcask.get(2) != null : "key 2 should not be null";
        assert SerializationUtils.toInteger(bitcask.get(2)) == 3 : "should equal 3";
        assert SerializationUtils.toInteger(bitcask.get(3)) == (count == 0 ? 8 : 10): "should equal 4";
        assert bitcask.get(128) != null : "128 is not null";
        assert SerializationUtils.toInteger(bitcask.get(128)) == 128 : "should equal 4";
        if(count == 0)
            assert bitcask.get(4) == null : "key 4: should equal null";

        bitcask.put(4, SerializationUtils.toBytes(5));
        assert SerializationUtils.toInteger(bitcask.get(4)) == 5 : "should equal 5";

        bitcask.put(3, SerializationUtils.toBytes(10));

        bitcask.sync();
       if(count == 0)
           helloRestart(1);
    }

    @Test
    void compactTest() {
        FileUtils.deleteFolder(path);

        int numRecord = 1000000;
        Bitcask bitcask = new Bitcask(path);

        for (int i = 0; i < numRecord; ++i) {
            bitcask.put(i, SerializationUtils.toBytes(i));
        }

        for (int i = 0; i < numRecord; ++i) {
            assert SerializationUtils.toInteger(bitcask.get(i)) == i : "should equal " + i;
        }

        List<Integer> deleted = new ArrayList<Integer>();
        List<Integer> exists = new ArrayList<Integer>();
        int count = 1;
        for (int i = 0; i < numRecord;) {
            for (int k = 0; k < count && i + k < numRecord; ++k) {
                 bitcask.delete(i + k);
                 deleted.add(i + k);
            }
            i += count;
            for (int k = 0; k < count && i + k < numRecord; ++k) {
                exists.add(i + k);
            }
            i += count;
            count += 1;
        }

        System.out.format("deleted item: %d, existed item: %d %n", deleted.size(), exists.size());

        System.out.format("before merge: %n%s %n", bitcask);

//        bitcask.merge();

        System.out.format("after merge: %n%s %n", bitcask);

        for (Integer key : exists) {
            assert bitcask.get(key) != null : "key: " + key + " is null";
            assert SerializationUtils.toInteger(bitcask.get((int)key)).equals(key) : "should equal " + key + ", but now is : " + SerializationUtils.toInteger(bitcask.get((int)key));
        }
        for (Integer key : deleted) {
            assert bitcask.get(key) == null : "should equal null";
        }

         compactionRestart(deleted, exists);
    }

    void compactionRestart(List<Integer> deleted, List<Integer> exists) {
        System.out.println("Compaction Restart");
        Bitcask bitcask = new Bitcask(path);
        System.out.format("After restart: %n%s", bitcask);
        for (Integer key : exists) {
            assert bitcask.get(key) != null && SerializationUtils.toInteger(bitcask.get((int)key)).equals(key)
                    : "should equal " + key;
        }
        for (Integer key : deleted) {
            assert bitcask.get(key) == null : "should equal null";
        }
    }

/**
multiple active file slot (4):
    Value: Integer
      1 thread : read/write throughput: 114.3 MB / 13.2 MB
      2 threads: read/write throughput: 114.9 MB / 18.7 MB
      4 threads: read/write throughput: 123.0 MB / 21.1 MB
    Value: byte[128]
      1 thread : read/write throughput: 399.2 MB / 146.6 MB
      2 threads: read/write throughput: 805.0 MB / 156.8 MB
      4 threads: read/write throughput: 775.5 MB / 233.2 MB
    Value: byte[512]
      4 threads: read/write throughput: 907.4 MB / 401.9 MB
    Value: byte[1024]
      4 threads: read/write throughput: 1692.0 MB / 574.4 MB
 **/
    @Test
    public void parallelWriteAndWrite() {
        FileUtils.deleteFolder(path);

        final boolean testCorrectioness = false;
        final int threadNum = 4;
        final int numRecord = 2000000;
        final int slice = (numRecord + threadNum - 1) / threadNum;
        Bitcask bitcask = new Bitcask(path);

        byte[] data = new byte[512];
//         byte[] data = SerializationUtils.toBytes(1);
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadNum; ++i) {
            final int thread_id = i;
            var thread = new Thread("" + i) {
                public void run() {
                    for (int k = slice * thread_id; k < slice * (thread_id + 1) && k < numRecord; ++k) {
//                        bitcask.put(k, SerializationUtils.toBytes(k));
                         bitcask.put(k, data);
                    }
                }
            };
            threads.add(thread);
        }

        long startTime = System.nanoTime();
        try {
            for (Thread t : threads) t.start();
            for (Thread t : threads) t.join();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        long stopTime = System.nanoTime();
        double elapsedTime = (stopTime - startTime) / 1000000000.0;
        System.out.format("write throughput: %.2fMB %n", numRecord / 1024.0 * (4 + data.length) / 1024.0 / elapsedTime);

        if(testCorrectioness) {
            for (int k = 0; k < numRecord; ++k) {
                assert SerializationUtils.toInteger(bitcask.get(k)) == k : "should equal " + k;
            }
        }

        threads.clear();
        for (int i = 0; i < threadNum; ++i) {
            final int thread_id = i;
            var t = new Thread("" + i) {
                public void run() {
                    for (int k = slice * thread_id; k < slice * (thread_id + 1) && k < numRecord; ++k) {
                        assert bitcask.get(k) != null : "is null " + k;
//                        assert SerializationUtils.toInteger(bitcask.get(k)) == k : "should equal " + k;
                    }
                }
            };
            threads.add(t);
        }

        startTime = System.nanoTime();
        try {
            for (Thread t : threads) t.start();
            for (Thread t : threads) t.join();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        stopTime = System.nanoTime();
        elapsedTime = (stopTime - startTime) / 1000000000.0;
        System.out.format("read throughput: %.2fMB %n", numRecord / 1024.0 * (4 + data.length) / 1024.0 / elapsedTime);
    }
}