package com.alphaxiao;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
         performanceTest();
    }

    // 0.6873 s
    public static void performanceTest() {
        long totalTime = 0;
        for(int k = 0; k < 5; ++k) {
            FileUtils.deleteFolder("./data/");
            Bitcask bitcask = new Bitcask("./data/");

            byte[] value = SerializationUtils.toBytes(1);
            long startTime = System.nanoTime();

            for (int i = 0; i < 100 * 10000; ++i) {
                bitcask.put(i, value);
                if (i % 1000 == 0) bitcask.sync();
            }
            long stopTime = System.nanoTime();
            totalTime += stopTime - startTime;
        }
        System.out.format("Total time: %.4f s", totalTime / 5.0 / 1000000000.0);
    }
}
