package com.alphaxiao;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class FileUtils {

    public static void deleteFile(String filename) {
        File file = new File(filename);
        if(!file.exists()) return;
        
        try {
            file.delete();
        } catch (Exception e) {
            System.out.format("Fail to delete file: %s, error: %s \n", filename, e.getMessage());
        }
    }
    
    public static void deleteFolder(String path) {
        if (new File(path).exists() == false)
            return;

        try {
            Files.walk(Paths.get(path)).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
        } catch (Exception e) {
            System.out.println("Fail to remove folder");
        } finally {

        }
    }
}
